package com.khomyshynets.bikesalesnotifier.telegram.library.exceptions;

public class TelegramComponentException extends RuntimeException {

    public TelegramComponentException(String message) {
        super(message);
    }

    public TelegramComponentException(String message, Throwable cause) {
        super(message, cause);
    }
}
