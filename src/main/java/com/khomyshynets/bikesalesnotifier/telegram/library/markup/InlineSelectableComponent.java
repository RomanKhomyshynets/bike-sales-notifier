package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageReplyMarkup;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import static com.khomyshynets.bikesalesnotifier.telegram.library.markup.ButtonEmojiHelper.*;

abstract class InlineSelectableComponent<THIS extends InlineSelectableComponent>
        extends AbstractButton<THIS, InlineMarkup, TelegramBotsInlineButton> {
    protected boolean selected;
    protected Emoji selectedEmoji;
    protected Emoji unselectedEmoji;

    protected InlineSelectableComponent(Long buttonId, String text) {
        super(buttonId, text);
        init();
    }

    protected InlineSelectableComponent(String text) {
        super(text);
        init();
    }

    @Override
    protected void createTelegramBotsButton() {
        this.telegramBotsButton = new TelegramBotsInlineButton();
        this.telegramBotsButton.setCallbackData(String.valueOf(this.id));
    }

    public boolean isSelected() {
        return selected;
    }

    public Emoji getSelectedEmoji() {
        return selectedEmoji;
    }

    @SuppressWarnings("unchecked")
    public THIS setSelectedEmoji(Emoji selectedEmoji) {
        this.selectedEmoji = selectedEmoji;
        return (THIS) this;
    }

    public Emoji getUnselectedEmoji() {
        return unselectedEmoji;
    }

    @SuppressWarnings("unchecked")
    public THIS setUnselectedEmoji(Emoji unselectedEmoji) {
        this.unselectedEmoji = unselectedEmoji;
        return (THIS) this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public THIS setEmoji(Emoji emoji, boolean isLeading) {
        this.setText(clearSelectionEmojies(this));
        super.setEmoji(emoji, isLeading);
        this.setText(pasteSelectionEmojies(this));
        return (THIS) this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public THIS removeEmoji() {
        this.setText(clearSelectionEmojies(this));
        this.setText((removeButtonEmojiFromText(this.getText(), this.emoji)));
        this.emoji = null;
        this.setText(pasteSelectionEmojies(this));
        return (THIS) this;
    }

    public abstract THIS setSelected(boolean selected);

    protected void updateMarkup(Update update, DefaultAbsSender bot) {
        EditMessageReplyMarkup editedMarkup = new EditMessageReplyMarkup();
        editedMarkup.setChatId(update.getCallbackQuery().getMessage().getChatId());
        editedMarkup.setMessageId(update.getCallbackQuery().getMessage().getMessageId());
        editedMarkup.setReplyMarkup(this.keyboardMarkup.toTelegramBotsMarkup());
        try {
            bot.execute(editedMarkup);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    protected abstract void init();
}
