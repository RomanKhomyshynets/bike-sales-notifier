package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

class ReplyMarkupBuildingWorker extends AbstractMarkupBuildingWorker<ReplyMarkup, ReplyButton> {

    ReplyMarkupBuildingWorker() {
        super(ReplyButton.class);
        this.keyboardMarkup = new ReplyMarkup()
                .setKeyboard(this.keyboard);
    }

    ReplyMarkupBuildingWorker(ReplyMarkup keyboardMarkup) {
        super(ReplyButton.class);
        this.keyboardMarkup = keyboardMarkup;
        this.keyboard = keyboardMarkup.getKeyboard();
    }

    void withRequestOfContact(Boolean requestContact) {
        ((ReplyButton) this.button).setRequestContact(requestContact);
    }

    void withRequestOfLocation(Boolean requestLocation) {
        ((ReplyButton) this.button).setRequestLocation(requestLocation);
    }

    void setResizeKeyboard(Boolean resizeKeyboard) {
        this.keyboardMarkup.setResizeKeyboard(resizeKeyboard);
    }

    void setOneTimeKeyboard(Boolean oneTimeKeyboard) {
        this.keyboardMarkup.setOneTimeKeyboard(oneTimeKeyboard);
    }

    void setSelective(Boolean selective) {
        this.keyboardMarkup.setSelective(selective);
    }
}


