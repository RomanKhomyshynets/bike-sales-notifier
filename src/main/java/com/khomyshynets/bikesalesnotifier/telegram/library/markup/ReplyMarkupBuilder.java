package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

public class ReplyMarkupBuilder extends AbstractMarkupBuilder<ReplyMarkupBuilder, ReplyMarkupBuildingWorker,
        ReplyButtonGridBuilder, ReplyButtonPropertiesBuilder, ReplyButtonBuilder, ReplyMarkup> {

    ReplyMarkupBuilder(ReplyMarkupBuildingWorker worker) {
        super(worker);
        this.buttonGridBuilder = new ReplyButtonGridBuilder(this.worker, this);
        this.buttonPropertiesBuilder = new ReplyButtonPropertiesBuilder(this.worker, this);
        this.buttonBuilder = new ReplyButtonBuilder(this.worker, this);
    }

    public ReplyMarkupBuilder setResizeKeyboard(Boolean resizeKeyboard) {
        this.worker.setResizeKeyboard(resizeKeyboard);
        return this;
    }

    public ReplyMarkupBuilder setOneTimeKeyboard(Boolean oneTimeKeyboard) {
        this.worker.setOneTimeKeyboard(oneTimeKeyboard);
        return this;
    }

    public ReplyMarkupBuilder setSelective(Boolean selective) {
        this.worker.setSelective(selective);
        return this;
    }
}
