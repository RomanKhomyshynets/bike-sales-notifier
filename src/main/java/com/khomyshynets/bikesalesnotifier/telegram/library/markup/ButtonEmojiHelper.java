package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

public class ButtonEmojiHelper {
    public static String pasteButtonEmojiIntoText(String sourceText, Emoji emoji, boolean isLeading) {
        return isLeading
                ? emoji.getUnicode() + " " + sourceText
                : sourceText + " " + emoji.getUnicode();
    }

    public static String removeButtonEmojiFromText(String sourceText, Emoji emoji) {
        return sourceText.replaceAll(emoji.getUnicode(), "");
    }

    public static String pasteSelectionEmojies(InlineSelectableComponent button) {
        return button.isSelected()
                ? pasteButtonEmojiIntoText(clearSelectionEmojies(button), button.getSelectedEmoji(), true)
                : pasteButtonEmojiIntoText(clearSelectionEmojies(button), button.getUnselectedEmoji(), true);
    }

    public static String clearSelectionEmojies(InlineSelectableComponent button) {
        return button.getText()
                .replaceAll(button.getSelectedEmoji().getUnicode(), "")
                .replaceAll(button.getUnselectedEmoji().getUnicode(), "");
    }
}
