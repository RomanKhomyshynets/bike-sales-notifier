package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import org.telegram.telegrambots.meta.api.interfaces.InputBotApiObject;

interface TelegramBotsButton {

    String getText();

    InputBotApiObject setText(String text);
}
