package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import com.khomyshynets.bikesalesnotifier.telegram.library.exceptions.TelegramComponentException;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.InputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static java.lang.Long.parseLong;

class WorkerBot {
    private boolean initialized;
    private Dialog<? extends AbstractMarkup> dialog;
    private DefaultAbsSender realBot;

    public WorkerBot(DefaultAbsSender realBot) {
        this.realBot = realBot;
    }

    void onUpdateReceived(Update update) {
        if (!this.initialized || (update.hasMessage() && update.getMessage().getText().equals("/start"))) {
            if (realBot instanceof EnhancedBot) {
                ((EnhancedBot)this.realBot).initialize(update);
            }
            this.initialized = true;
        }

        Optional<List<List<? extends AbstractButton>>> keyboard = Optional.ofNullable(this.dialog)
                .map(Dialog::getMarkup)
                .map(AbstractMarkup::getKeyboard);
        Optional<? extends AbstractButton> component = Optional.empty();

        if (keyboard.isPresent()) {
            if (update.hasCallbackQuery()) {
                try {
                    Long inlineComponentId = parseLong(update.getCallbackQuery().getData());
                    component = findComponent(keyboard.get(), inlineComponent -> inlineComponent.getId().equals(inlineComponentId));
                } catch (NumberFormatException ignored) {
                }
            } else {
                component = findComponent(keyboard.get(), replyComponent -> replyComponent.getText().equals(update.getMessage().getText()));
            }
        }

        component.ifPresent(comp -> comp.updateReceived(update, this.realBot));
    }

    Message sendDialog(Long chatId, Dialog<? extends AbstractMarkup> dialog) {
        this.dialog = dialog;
        SendMessage message = new SendMessage();
        message.setChatId(chatId);

        Optional.ofNullable(dialog.getText())
                .ifPresent(message::setText);

        Optional.ofNullable(dialog.getMarkup())
                .filter(markup -> markup.getClass().equals(InlineMarkup.class))
                .map(AbstractMarkup::getKeyboard)
                .ifPresent(this::validateInlineKeyboardIds);

        Optional.ofNullable(dialog.getMarkup())
                .ifPresent(markup -> message.setReplyMarkup(markup.toTelegramBotsMarkup()));

        try {
            return this.realBot.execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
            return new Message();
        }
    }

    Message sendText(Long chatId, String text) {
        SendMessage message = new SendMessage();
        message.setChatId(chatId);
        message.setText(text);
        try {
           return this.realBot.execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
            return new Message();
        }
    }

    Message sendPhoto(Long chatId, String photoName, InputStream photoInputStream) {
        SendPhoto photo = new SendPhoto();
        photo.setChatId(chatId);
        photo.setPhoto(photoName, photoInputStream);
        try {
            return this.realBot.execute(photo);
        } catch (TelegramApiException e) {
            e.printStackTrace();
            return new Message();
        }
    }

    private Optional<? extends AbstractButton> findComponent(List<List<? extends AbstractButton>> keyboard, Predicate<? super AbstractButton> predicate) {
        return keyboard.stream()
                .flatMap(Collection::stream)
                .filter(predicate)
                .findFirst();
    }

    private void validateInlineKeyboardIds(List<List<? extends AbstractButton>> keyboard) {
        if (!keyboard.stream()
                .flatMap(Collection::stream)
                .map(AbstractButton::getId)
                .allMatch(new HashSet<>()::add)) {
            throw new TelegramComponentException("Components of inline keyboard must have unique IDs");
        }
    }
}
