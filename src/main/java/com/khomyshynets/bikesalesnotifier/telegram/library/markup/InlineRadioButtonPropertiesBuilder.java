package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

public class InlineRadioButtonPropertiesBuilder extends AbstractButtonPropertiesBuilder<InlineRadioButtonPropertiesBuilder,
        InlineMarkupBuildingWorker, InlineMarkupBuilder, InlineButtonPropertiesBuilder, InlineRadioButton> {

    InlineRadioButtonPropertiesBuilder(InlineMarkupBuildingWorker worker, InlineMarkupBuilder markupBuilder) {
        super(worker, markupBuilder);
    }

    public InlineCheckboxPropertiesBuilder addCheckbox(String text) {
        this.worker.addCheckbox(text);
        return this.markupBuilder.getCheckboxPropertiesBuilder();
    }

    public InlineCheckboxPropertiesBuilder addCheckbox(Long buttonId, String text) {
        this.worker.addCheckbox(buttonId, text);
        return this.markupBuilder.getCheckboxPropertiesBuilder();
    }

    public InlineRadioButtonPropertiesBuilder addRadioButton(String text, ButtonGroup<InlineRadioButton> buttonGroup) {
        this.worker.addRadioButton(text, buttonGroup);
        return this;
    }

    public InlineRadioButtonPropertiesBuilder addRadioButton(Long buttonId, String text, ButtonGroup<InlineRadioButton> buttonGroup) {
        this.worker.addRadioButton(buttonId, text, buttonGroup);
        return this;
    }
}
