package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

public class Builders {
    public static InlineMarkupBuilder inlineMarkupBuilder() {
        return new InlineMarkupBuilder(new InlineMarkupBuildingWorker());
    }

    public static ReplyMarkupBuilder replyMarkupBuilder() {
        return new ReplyMarkupBuilder(new ReplyMarkupBuildingWorker());
    }
}
