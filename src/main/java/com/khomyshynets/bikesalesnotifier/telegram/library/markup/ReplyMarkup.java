package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

public class ReplyMarkup extends AbstractMarkup<ReplyMarkup, ReplyButton, ReplyKeyboardMarkup> {

    public ReplyMarkup() {
        super(ReplyButton.class);
        this.telegramBotsMarkup = new ReplyKeyboardMarkup();
    }

    public ReplyMarkup(ReplyKeyboardMarkup telegramBotsMarkup) {
        super(ReplyButton.class, telegramBotsMarkup);
    }

    @Override
    protected void fillKeyboard() {
        this.keyboard = this.telegramBotsMarkup.getKeyboard()
                .stream()
                .map(row -> row.stream()
                        .map(telegramBotsButton -> new ReplyButton(telegramBotsButton.getText())
                                .setTelegramBotsButton(new TelegramBotsReplyButton(telegramBotsButton)))
                        .collect(toList()))
                .collect(toList());
    }

    @Override
    protected void fillTelegramBotsKeyboard() {
        this.telegramBotsMarkup.setKeyboard(this.keyboard.stream()
                .map(row -> row.stream()
                        .map(abstractButton -> (KeyboardButton) abstractButton.getTelegramBotsButton())
                        .collect(toCollection(KeyboardRow::new)))
                .collect(toList()));
    }

    public ReplyMarkupBuilder getBuilder() {
        return new ReplyMarkupBuilder(new ReplyMarkupBuildingWorker(this));
    }

    public void setResizeKeyboard(Boolean resizeKeyboard) {
        this.telegramBotsMarkup.setResizeKeyboard(resizeKeyboard);
    }

    public void setOneTimeKeyboard(Boolean oneTimeKeyboard) {
        this.telegramBotsMarkup.setOneTimeKeyboard(oneTimeKeyboard);
    }

    public void setSelective(Boolean selective) {
        this.telegramBotsMarkup.setSelective(selective);
    }
}
