package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

public class ReplyButton extends AbstractButton<ReplyButton, ReplyMarkup, TelegramBotsReplyButton> {

    public ReplyButton(Long id, String text) {
        super(id, text);
    }

    public ReplyButton(String text) {
        super(text);
    }

    @Override
    protected void createTelegramBotsButton() {
        this.telegramBotsButton = new TelegramBotsReplyButton();
    }

    public Boolean getRequestContact() {
        return this.telegramBotsButton.getRequestContact();
    }

    public ReplyButton setRequestContact(Boolean requestContact) {
        this.telegramBotsButton.setRequestContact(requestContact);
        return this;
    }

    public Boolean getRequestLocation() {
        return this.telegramBotsButton.getRequestLocation();
    }

    public ReplyButton setRequestLocation(Boolean requestLocation) {
        this.telegramBotsButton.setRequestLocation(requestLocation);
        return this;
    }
}

