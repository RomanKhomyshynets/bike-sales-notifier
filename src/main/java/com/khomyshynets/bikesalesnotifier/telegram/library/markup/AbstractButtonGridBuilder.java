package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import com.khomyshynets.bikesalesnotifier.telegram.library.exceptions.TelegramComponentException;

import java.util.List;
import java.util.Map;

abstract class AbstractButtonGridBuilder<THIS extends AbstractButtonGridBuilder,
        WORKER extends AbstractMarkupBuildingWorker,
        MARKUP_BUILDER extends AbstractMarkupBuilder,
        BUTTON_BUILDER extends AbstractButtonBuilder,
        BUTTON_GRID_BUILDER extends AbstractButtonGridBuilder,
        COMPONENT extends AbstractButton,
        MARKUP extends AbstractMarkup> {
    protected WORKER worker;
    protected MARKUP_BUILDER markupBuilder;

    protected AbstractButtonGridBuilder(WORKER worker, MARKUP_BUILDER markupBuilder) {
        this.worker = worker;
        this.markupBuilder = markupBuilder;
    }

    @SuppressWarnings("unchecked")
    public BUTTON_BUILDER startRow() {
        this.worker.startRow();
        return (BUTTON_BUILDER) this.markupBuilder.getButtonBuilder();
    }

    @SuppressWarnings("unchecked")
    public BUTTON_GRID_BUILDER addButtonGrid(List<String> buttonTexts, int columnAmount) {
        this.worker.addButtonGrid(buttonTexts, columnAmount);
        return (BUTTON_GRID_BUILDER) this.markupBuilder.getButtonGridBuilder();
    }

    @SuppressWarnings("unchecked")
    public BUTTON_GRID_BUILDER addButtonGrid(Map<Long, String> buttonInfo, int columnAmount) {
        this.worker.addButtonGrid(buttonInfo, columnAmount);
        return (BUTTON_GRID_BUILDER) this.markupBuilder.getButtonGridBuilder();
    }

    @SuppressWarnings("unchecked")
    public THIS withEmojiForEachButton(String emojiShortCode, boolean isLeading) {
        this.worker.withEmojiForEachButton(emojiShortCode, isLeading);
        return (THIS) this;
    }

    @SuppressWarnings("unchecked")
    public THIS withButtonGroupForEachButton(ButtonGroup<COMPONENT> buttonGroup) {
        this.worker.withButtonGroupForEachButton(buttonGroup);
        return (THIS) this;
    }

    @SuppressWarnings("unchecked")
    public THIS withActionListenerForEachButton(ActionListener<COMPONENT> actionListener) {
        this.worker.withActionListenerForEachButton(actionListener);
        return (THIS) this;
    }

    public MARKUP_BUILDER removeButton(Long buttonId) throws TelegramComponentException {
        this.worker.removeButton(buttonId);
        return this.markupBuilder;
    }

    public MARKUP_BUILDER removeButtons(List<Long> buttonIds) throws TelegramComponentException {
        this.worker.removeButtons(buttonIds);
        return this.markupBuilder;
    }

    public MARKUP_BUILDER removeRow(int rowNumber) throws TelegramComponentException {
        this.worker.removeRow(rowNumber);
        return this.markupBuilder;
    }

    @SuppressWarnings("unchecked")
    public MARKUP build() {
        return (MARKUP) this.worker.build();
    }
}
