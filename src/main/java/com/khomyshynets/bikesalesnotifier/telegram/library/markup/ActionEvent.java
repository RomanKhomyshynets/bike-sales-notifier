package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.objects.Update;

public class ActionEvent<T extends AbstractButton> {
    private T source;
    private Update update;
    private DefaultAbsSender bot;

    public ActionEvent(T source, Update update, DefaultAbsSender bot) {
        this.source = source;
        this.update = update;
        this.bot = bot;
    }

    public T getSource() {
        return source;
    }

    public Update getUpdate() {
        return update;
    }

    public DefaultAbsSender getBot() {
        return bot;
    }
}
