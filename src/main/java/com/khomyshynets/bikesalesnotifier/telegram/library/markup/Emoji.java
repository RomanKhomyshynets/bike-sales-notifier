package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import com.vdurmont.emoji.EmojiParser;

public class Emoji {
    private String unicode;
    private String shortCode;

    public Emoji(String shortCode) {
        this.shortCode = shortCode;
        this.unicode = EmojiParser.parseToUnicode(shortCode);
    }

    public String getUnicode() {
        return unicode;
    }

    public String getShortCode() {
        return shortCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Emoji that = (Emoji) o;

        if (!unicode.equals(that.unicode)) return false;
        return shortCode.equals(that.shortCode);
    }

    @Override
    public int hashCode() {
        int result = unicode.hashCode();
        result = 31 * result + shortCode.hashCode();
        return result;
    }
}
