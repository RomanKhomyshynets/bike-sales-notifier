package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

class TelegramBotsInlineButton extends InlineKeyboardButton implements TelegramBotsButton {

    TelegramBotsInlineButton() {
    }

    TelegramBotsInlineButton(InlineKeyboardButton inlineKeyboardButton) {
        this.setText(inlineKeyboardButton.getText());
        this.setPay(inlineKeyboardButton.getPay());
        this.setCallbackGame(inlineKeyboardButton.getCallbackGame());
        this.setSwitchInlineQuery(inlineKeyboardButton.getSwitchInlineQuery());
        this.setSwitchInlineQueryCurrentChat(inlineKeyboardButton.getSwitchInlineQueryCurrentChat());
        this.setCallbackData(inlineKeyboardButton.getCallbackData());
        this.setUrl(inlineKeyboardButton.getUrl());
    }
}
