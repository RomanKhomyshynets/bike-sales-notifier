package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

public class Dialog<T extends AbstractMarkup> {
    String text;
    private T markup;

    public Dialog() {
    }

    public Dialog(String text) {
        this.text = text;
    }

    public Dialog(String text, T markup) {
        this.text = text;
        this.markup = markup;
    }

    public String getText() {
        return text;
    }

    public Dialog setText(String text) {
        this.text = text;
        return this;
    }

    public T getMarkup() {
        return markup;
    }

    public Dialog setMarkup(T markup) {
        this.markup = markup;
        return this;
    }
}
