package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import com.khomyshynets.bikesalesnotifier.telegram.library.exceptions.TelegramComponentException;

import java.util.List;
import java.util.Map;

abstract class AbstractMarkupBuilder<THIS extends AbstractMarkupBuilder,
        WORKER extends AbstractMarkupBuildingWorker,
        BUTTON_GRID_BUILDER extends AbstractButtonGridBuilder,
        BUTTON_PROPERTIES_BUILDER extends AbstractButtonPropertiesBuilder,
        BUTTON_BUILDER extends AbstractButtonBuilder,
        MARKUP extends AbstractMarkup> {
    protected WORKER worker;
    protected BUTTON_GRID_BUILDER buttonGridBuilder;
    protected BUTTON_PROPERTIES_BUILDER buttonPropertiesBuilder;
    protected BUTTON_BUILDER buttonBuilder;

    protected AbstractMarkupBuilder(WORKER worker) {
        this.worker = worker;
    }

    BUTTON_GRID_BUILDER getButtonGridBuilder() {
        return buttonGridBuilder;
    }

    BUTTON_PROPERTIES_BUILDER getButtonPropertiesBuilder() {
        return buttonPropertiesBuilder;
    }

    BUTTON_BUILDER getButtonBuilder() {
        return buttonBuilder;
    }

    public BUTTON_BUILDER startRow() {
        this.worker.startRow();
        return this.buttonBuilder;
    }

    public BUTTON_GRID_BUILDER addButtonGrid(List<String> buttonTexts, int columnAmount) {
        this.worker.addButtonGrid(buttonTexts, columnAmount);
        return this.buttonGridBuilder;
    }

    public BUTTON_GRID_BUILDER addButtonGrid(Map<Long, String> buttonInfo, int columnAmount) {
        this.worker.addButtonGrid(buttonInfo, columnAmount);
        return this.buttonGridBuilder;
    }

    @SuppressWarnings("unchecked")
    public THIS removeButton(Long buttonId) throws TelegramComponentException {
        this.worker.removeButton(buttonId);
        return (THIS) this;
    }

    @SuppressWarnings("unchecked")
    public THIS removeButtons(List<Long> buttonIds) throws TelegramComponentException {
        this.worker.removeButtons(buttonIds);
        return (THIS) this;
    }

    @SuppressWarnings("unchecked")
    public THIS removeRow(int rowNumber) throws TelegramComponentException {
        this.worker.removeRow(rowNumber);
        return (THIS) this;
    }

    @SuppressWarnings("unchecked")
    public MARKUP build() {
        return (MARKUP) this.worker.build();
    }
}
