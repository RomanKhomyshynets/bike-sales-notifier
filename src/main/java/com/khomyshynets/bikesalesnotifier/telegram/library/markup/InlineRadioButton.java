package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.objects.Update;

import static com.khomyshynets.bikesalesnotifier.telegram.library.markup.ButtonEmojiHelper.pasteButtonEmojiIntoText;

public class InlineRadioButton extends InlineSelectableComponent<InlineRadioButton> {

    public InlineRadioButton(Long buttonId, String text) {
        super(buttonId, text);
    }

    public InlineRadioButton(String text) {
        super(text);
    }

    @Override
    public InlineRadioButton setSelected(boolean selected) {
        this.setText(selected
                ? pasteButtonEmojiIntoText(ButtonEmojiHelper.clearSelectionEmojies(this),
                this.selectedEmoji, true)
                : pasteButtonEmojiIntoText(ButtonEmojiHelper.clearSelectionEmojies(this),
                this.unselectedEmoji, true));
        if (this.buttonGroup != null) {
            if (selected) {
                this.buttonGroup.getSelectedElements()
                        .forEach(element -> element.setSelected(false));
                this.buttonGroup.removeFromSelected(this.buttonGroup.getSelectedElements());
                this.buttonGroup.addToSelected(this);
            }
        }
        this.selected = selected;
        return this;
    }

    @Override
    public void updateReceived(Update update, DefaultAbsSender bot) {
        super.updateReceived(update, bot);
        if (!this.selected) {
            this.setSelected(true);
            updateMarkup(update, bot);
        }
    }

    @Override
    protected void init() {
        selectedEmoji = new Emoji(":black_circle:");
        unselectedEmoji = new Emoji(":white_circle:");
        this.setText(pasteButtonEmojiIntoText(this.getText(), this.unselectedEmoji, true));
    }
}
