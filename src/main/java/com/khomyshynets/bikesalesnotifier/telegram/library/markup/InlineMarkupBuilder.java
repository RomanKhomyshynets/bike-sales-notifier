package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import com.khomyshynets.bikesalesnotifier.telegram.library.exceptions.TelegramComponentException;

import java.util.List;
import java.util.Map;

public class InlineMarkupBuilder extends AbstractMarkupBuilder<InlineMarkupBuilder, InlineMarkupBuildingWorker,
        InlineButtonGridBuilder, InlineButtonPropertiesBuilder, InlineButtonBuilder, InlineMarkup> {
    private InlineCheckboxGridBuilder checkboxGridBuilder;
    private InlineRadioButtonGridBuilder radioButtonGridBuilder;
    private InlineCheckboxPropertiesBuilder checkboxPropertiesBuilder;
    private InlineRadioButtonPropertiesBuilder radioButtonPropertiesBuilder;

    InlineMarkupBuilder(InlineMarkupBuildingWorker worker) {
        super(worker);
        this.buttonGridBuilder = new InlineButtonGridBuilder(worker, this);
        this.checkboxGridBuilder = new InlineCheckboxGridBuilder(worker, this);
        this.radioButtonGridBuilder = new InlineRadioButtonGridBuilder(worker, this);
        this.buttonPropertiesBuilder = new InlineButtonPropertiesBuilder(worker, this);
        this.checkboxPropertiesBuilder = new InlineCheckboxPropertiesBuilder(worker, this);
        this.radioButtonPropertiesBuilder = new InlineRadioButtonPropertiesBuilder(worker, this);
        this.buttonBuilder = new InlineButtonBuilder(this.worker, this);
    }

    InlineCheckboxGridBuilder getCheckboxGridBuilder() {
        return this.checkboxGridBuilder;
    }

    InlineRadioButtonGridBuilder getRadioButtonGridBuilder() {
        return this.radioButtonGridBuilder;
    }

    InlineCheckboxPropertiesBuilder getCheckboxPropertiesBuilder() {
        return this.checkboxPropertiesBuilder;
    }

    InlineRadioButtonPropertiesBuilder getRadioButtonPropertiesBuilder() {
        return this.radioButtonPropertiesBuilder;
    }

    public InlineCheckboxGridBuilder addCheckboxGrid(List<String> checkboxTexts, int columnAmount) {
        this.worker.addCheckboxGrid(checkboxTexts, columnAmount);
        return this.checkboxGridBuilder;
    }

    public InlineCheckboxGridBuilder addCheckboxGrid(Map<Long, String> checkboxInfo, int columnAmount) {
        this.worker.addCheckboxGrid(checkboxInfo, columnAmount);
        return this.checkboxGridBuilder;
    }

    public InlineRadioButtonGridBuilder addRadioButtonGrid(List<String> radioButtonTexts, int columnAmount) {
        this.worker.addRadioButtonGrid(radioButtonTexts, columnAmount);
        return this.radioButtonGridBuilder;
    }

    public InlineRadioButtonGridBuilder addRadioButtonGrid(Map<Long, String> radioButtonInfo, int columnAmount) {
        this.worker.addRadioButtonGrid(radioButtonInfo, columnAmount);
        return this.radioButtonGridBuilder;
    }

    public InlineMarkupBuilder removeCheckbox(Long buttonId) throws TelegramComponentException {
        this.worker.removeCheckbox(buttonId);
        return this;
    }

    public InlineMarkupBuilder removeRadioButton(Long buttonId) throws TelegramComponentException {
        this.worker.removeRadioButton(buttonId);
        return this;
    }

    public InlineMarkupBuilder removeCheckboxes(List<Long> buttonIds) throws TelegramComponentException {
        this.worker.removeCheckboxes(buttonIds);
        return this;
    }

    public InlineMarkupBuilder removeRadioButtons(List<Long> buttonIds) throws TelegramComponentException {
        this.worker.removeRadioButtons(buttonIds);
        return this;
    }
}
