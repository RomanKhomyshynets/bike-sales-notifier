package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import static com.khomyshynets.bikesalesnotifier.telegram.library.markup.ButtonEmojiHelper.pasteButtonEmojiIntoText;
import static com.khomyshynets.bikesalesnotifier.telegram.library.markup.ButtonEmojiHelper.removeButtonEmojiFromText;

abstract class AbstractButton<THIS extends AbstractButton,
        MARKUP extends AbstractMarkup,
        API_BUTTON extends TelegramBotsButton> {
    protected Long id;
    protected MARKUP keyboardMarkup;
    protected Emoji emoji;
    protected ButtonGroup<THIS> buttonGroup;
    protected API_BUTTON telegramBotsButton;
    private List<ActionListener<THIS>> actionListeners = new ArrayList<>();

    private final Long BUTTON_ID_MIN_VALUE = 1_000_000L;
    private final Long BUTTON_ID_MAX_VALUE = 9_999_999L;

    protected AbstractButton(Long id, String text) {
        this.id = id;
        createTelegramBotsButton();
        this.setText(text);
    }

    protected AbstractButton(String text) {
        this.id = ThreadLocalRandom.current().nextLong(BUTTON_ID_MIN_VALUE, BUTTON_ID_MAX_VALUE + 1);
        createTelegramBotsButton();
        this.setText(text);
    }

    protected abstract void createTelegramBotsButton();

    API_BUTTON getTelegramBotsButton() {
        return telegramBotsButton;
    }

    @SuppressWarnings("unchecked")
    THIS setTelegramBotsButton(API_BUTTON telegramBotsButton) {
        this.telegramBotsButton = telegramBotsButton;
        return (THIS) this;
    }

    public String getText() {
        return this.telegramBotsButton.getText();
    }

    @SuppressWarnings("unchecked")
    public THIS setText(String text) {
        this.telegramBotsButton.setText(text);
        return (THIS) this;
    }

    public Long getId() {
        return id;
    }

    @SuppressWarnings("unchecked")
    public THIS setId(Long id) {
        this.id = id;
        return (THIS) this;
    }

    public MARKUP getKeyboardMarkup() {
        return keyboardMarkup;
    }

    @SuppressWarnings("unchecked")
    THIS setKeyboardMarkup(MARKUP keyboardMarkup) {
        this.keyboardMarkup = keyboardMarkup;
        return (THIS) this;
    }

    public Emoji getEmoji() {
        return emoji;
    }

    @SuppressWarnings("unchecked")
    public THIS setEmoji(Emoji emoji, boolean isLeading) {
        if (this.emoji != null) {
            this.setText(removeButtonEmojiFromText(this.getText(), this.emoji));
        }
        this.emoji = emoji;
        if (this.emoji != null) {
            this.setText(pasteButtonEmojiIntoText(this.getText(), this.emoji, isLeading));
        }
        return (THIS) this;
    }

    @SuppressWarnings("unchecked")
    public THIS removeEmoji() {
        this.setText((removeButtonEmojiFromText(this.getText(), this.emoji)));
        this.emoji = null;
        return (THIS) this;
    }

    public Optional<ButtonGroup<THIS>> getButtonGroup() {
        return Optional.ofNullable(buttonGroup);
    }

    @SuppressWarnings("unchecked")
    THIS setButtonGroup(ButtonGroup<THIS> buttonGroup) {
        this.buttonGroup = buttonGroup;
        return (THIS) this;
    }

    @SuppressWarnings("unchecked")
    public THIS addToButtonGroup(ButtonGroup<THIS> buttonGroup) {
        buttonGroup.add((THIS) this);
        return (THIS) this;
    }

    @SuppressWarnings("unchecked")
    public THIS removeFromButtonGroup(ButtonGroup<THIS> buttonGroup) {
        buttonGroup.remove((THIS) this);
        return (THIS) this;
    }

    @SuppressWarnings("unchecked")
    public THIS addActionListener(ActionListener<THIS> listener) {
        this.actionListeners.add(listener);
        return (THIS) this;
    }

    @SuppressWarnings("unchecked")
    public THIS removeActionListener(ActionListener<THIS> listener) {
        this.actionListeners.remove(listener);
        return (THIS) this;
    }

    @SuppressWarnings("unchecked")
    private void fireActionPerformed(ActionEvent<THIS> e) {
        actionListeners.forEach(listener -> listener.actionPerformed(e));
    }

    @SuppressWarnings("unchecked")
    public void updateReceived(Update update, DefaultAbsSender bot) {
        ActionEvent<THIS> e = new ActionEvent<>((THIS) this, update, bot);
        fireActionPerformed(e);
    }
}
