package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

public class ReplyButtonBuilder extends AbstractButtonBuilder<ReplyMarkupBuildingWorker, ReplyMarkupBuilder,
        ReplyButtonPropertiesBuilder> {

    ReplyButtonBuilder(ReplyMarkupBuildingWorker worker, ReplyMarkupBuilder markupBuilder) {
        super(worker, markupBuilder);
    }
}
