package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

@FunctionalInterface
public interface ActionListener<T extends AbstractButton> {
    void actionPerformed(ActionEvent<T> e);
}
