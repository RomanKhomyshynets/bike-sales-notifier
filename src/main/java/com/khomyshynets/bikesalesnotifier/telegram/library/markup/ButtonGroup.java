package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import java.util.*;

public class ButtonGroup<T extends AbstractButton> {
    private Set<T> elements = new HashSet<>();
    private Set<T> selectedElements = new HashSet<>();

    public ButtonGroup() {
    }

    public ButtonGroup(List<T> elements) {
        this.add(elements);
    }

    public Set<T> getElements() {
        return Collections.unmodifiableSet(elements);
    }

    public Set<T> getSelectedElements() {
        return Collections.unmodifiableSet(selectedElements);
    }

    @SuppressWarnings("unchecked")
    public ButtonGroup<T> add(T element) {
        this.elements.add(element);
        element.setButtonGroup(this);
        return this;
    }

    @SuppressWarnings("unchecked")
    public ButtonGroup<T> add(Collection<? extends T> elements) {
        this.elements.addAll(elements);
        elements.forEach(element -> element.setButtonGroup(this));
        return this;
    }

    @SuppressWarnings("unchecked")
    public ButtonGroup<T> remove(T element) {
        this.elements.remove(element);
        element.setButtonGroup(null);
        return this;
    }

    @SuppressWarnings("unchecked")
    public ButtonGroup<T> remove(Collection<? extends T> elements) {
        this.elements.removeAll(elements);
        elements.forEach(element -> element.setButtonGroup(null));
        return this;
    }

    void addToSelected(T element) {
        this.selectedElements.add(element);
    }

    void addToSelected(Collection<T> elements) {
        this.selectedElements.addAll(elements);
    }

    void removeFromSelected(T element) {
        this.selectedElements.remove(element);
    }

    void removeFromSelected(Collection<T> elements) {
        this.selectedElements.removeAll(elements);
    }
}
