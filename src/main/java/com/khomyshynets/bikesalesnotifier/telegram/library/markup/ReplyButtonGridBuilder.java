package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

public class ReplyButtonGridBuilder extends AbstractButtonGridBuilder<ReplyButtonGridBuilder, ReplyMarkupBuildingWorker,
        ReplyMarkupBuilder, ReplyButtonBuilder, ReplyButtonGridBuilder, ReplyButton, ReplyMarkup> {

    ReplyButtonGridBuilder(ReplyMarkupBuildingWorker worker, ReplyMarkupBuilder markupBuilder) {
        super(worker, markupBuilder);
    }

    public ReplyMarkupBuilder setResizeKeyboard(Boolean resizeKeyboard) {
        this.worker.setResizeKeyboard(resizeKeyboard);
        return this.markupBuilder;
    }

    public ReplyMarkupBuilder setOneTimeKeyboard(Boolean oneTimeKeyboard) {
        this.worker.setOneTimeKeyboard(oneTimeKeyboard);
        return this.markupBuilder;
    }

    public ReplyMarkupBuilder setSelective(Boolean selective) {
        this.worker.setSelective(selective);
        return this.markupBuilder;
    }
}
