package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import com.khomyshynets.bikesalesnotifier.telegram.library.exceptions.TelegramComponentException;
import org.telegram.telegrambots.meta.api.objects.games.CallbackGame;

public class InlineButtonPropertiesBuilder extends AbstractButtonPropertiesBuilder<InlineButtonPropertiesBuilder,
        InlineMarkupBuildingWorker, InlineMarkupBuilder, InlineButtonPropertiesBuilder, InlineButton> {

    InlineButtonPropertiesBuilder(InlineMarkupBuildingWorker worker, InlineMarkupBuilder markupBuilder) {
        super(worker, markupBuilder);
    }

    public InlineCheckboxPropertiesBuilder addCheckbox(String text) {
        this.worker.addCheckbox(text);
        return this.markupBuilder.getCheckboxPropertiesBuilder();
    }

    public InlineCheckboxPropertiesBuilder addCheckbox(Long buttonId, String text) {
        this.worker.addCheckbox(buttonId, text);
        return this.markupBuilder.getCheckboxPropertiesBuilder();
    }

    public InlineRadioButtonPropertiesBuilder addRadioButton(String text, ButtonGroup<InlineRadioButton> buttonGroup) {
        this.worker.addRadioButton(text, buttonGroup);
        return this.markupBuilder.getRadioButtonPropertiesBuilder();
    }

    public InlineRadioButtonPropertiesBuilder addRadioButton(Long buttonId, String text, ButtonGroup<InlineRadioButton> buttonGroup) {
        this.worker.addRadioButton(buttonId, text, buttonGroup);
        return this.markupBuilder.getRadioButtonPropertiesBuilder();
    }

    public InlineButtonPropertiesBuilder withUrl(String url) throws TelegramComponentException {
        this.worker.withUrl(url);
        return this;
    }

    public InlineButtonPropertiesBuilder withSwitchingInlineQuery(String switchInlineQuery)
            throws TelegramComponentException {
        this.worker.withSwitchingInlineQuery(switchInlineQuery);
        return this;
    }

    public InlineButtonPropertiesBuilder withSwitchingInlineQueryInCurrentChat(String switchInlineQueryCurrentChat)
            throws TelegramComponentException {
        this.worker.withSwitchingInlineQueryInCurrentChat(switchInlineQueryCurrentChat);
        return this;
    }

    public InlineButtonPropertiesBuilder withCallbackGame(CallbackGame callbackGame) throws TelegramComponentException {
        this.worker.withCallbackGame(callbackGame);
        return this;
    }

    public InlineButtonPropertiesBuilder withPayment(Boolean pay) throws TelegramComponentException {
        this.worker.withPayment(pay);
        return this;
    }
}
