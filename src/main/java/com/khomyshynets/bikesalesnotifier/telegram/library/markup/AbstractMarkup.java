package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;

abstract class AbstractMarkup<THIS extends AbstractMarkup,
        BUTTON extends AbstractButton,
        API_MARKUP extends ReplyKeyboard> {
    protected List<List<? extends AbstractButton>> keyboard;
    protected API_MARKUP telegramBotsMarkup;
    private Class<BUTTON> buttonType;

    protected AbstractMarkup(Class<BUTTON> buttonType) {
        this.buttonType = buttonType;
    }

    protected AbstractMarkup(Class<BUTTON> buttonType, API_MARKUP telegramBotsMarkup) {
        this(buttonType);
        this.telegramBotsMarkup = telegramBotsMarkup;
        fillKeyboard();
    }

    protected abstract void fillKeyboard();

    protected abstract void fillTelegramBotsKeyboard();

    public API_MARKUP toTelegramBotsMarkup() {
        fillTelegramBotsKeyboard();
        return this.telegramBotsMarkup;
    }

    public Optional<BUTTON> getButtonById(Long buttonId) {
        return this.getAllButtons()
                .filter(button -> button.getId().equals(buttonId))
                .findFirst();
    }

    public Optional<BUTTON> getButtonByText(String buttonText) {
        return this.getAllButtons()
                .filter(button -> button.getText().equals(buttonText))
                .findFirst();
    }

    @SuppressWarnings("unchecked")
    public Stream<BUTTON> getAllButtons() {
        return this.keyboard
                .stream()
                .flatMap(Collection::stream)
                .filter(button -> button.getClass().equals(this.buttonType))
                .map(button -> (BUTTON) button);
    }

    public void forEachButton(Consumer<BUTTON> consumer) {
        this.getAllButtons().forEach(consumer);
    }

    public List<List<? extends AbstractButton>> getKeyboard() {
        return this.keyboard;
    }

    @SuppressWarnings("unchecked")
    public THIS setKeyboard(List<? extends List<? extends AbstractButton>> keyboard) {
        keyboard.stream()
                .flatMap(Collection::stream)
                .forEach(button -> button.setKeyboardMarkup(this));

        this.keyboard = (List<List<? extends AbstractButton>>) keyboard;
        return (THIS) this;
    }
}
