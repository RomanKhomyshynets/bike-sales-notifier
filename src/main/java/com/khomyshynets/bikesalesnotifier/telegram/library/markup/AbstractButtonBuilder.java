package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

abstract class AbstractButtonBuilder<WORKER extends AbstractMarkupBuildingWorker,
        MARKUP_BUILDER extends AbstractMarkupBuilder,
        BUTTON_PROPERTIES_BUILDER extends AbstractButtonPropertiesBuilder> {
    protected WORKER worker;
    protected MARKUP_BUILDER markupBuilder;

    protected AbstractButtonBuilder(WORKER worker, MARKUP_BUILDER markupBuilder) {
        this.worker = worker;
        this.markupBuilder = markupBuilder;
    }

    @SuppressWarnings("unchecked")
    public BUTTON_PROPERTIES_BUILDER addButton(String text) {
        this.worker.addButton(text);
        return (BUTTON_PROPERTIES_BUILDER) this.markupBuilder.getButtonPropertiesBuilder();
    }

    @SuppressWarnings("unchecked")
    public BUTTON_PROPERTIES_BUILDER addButton(Long buttonId, String text) {
        this.worker.addButton(buttonId, text);
        return (BUTTON_PROPERTIES_BUILDER) this.markupBuilder.getButtonPropertiesBuilder();
    }

    public MARKUP_BUILDER endRow() {
        this.worker.endRow();
        return this.markupBuilder;
    }
}
