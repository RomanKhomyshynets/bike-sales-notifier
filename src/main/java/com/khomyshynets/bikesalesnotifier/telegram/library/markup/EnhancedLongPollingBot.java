package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.InputStream;

public abstract class EnhancedLongPollingBot extends TelegramLongPollingBot implements EnhancedBot {
    private final WorkerBot workerBot = new WorkerBot(this);

    @Override
    public void onUpdateReceived(Update update) {
        this.workerBot.onUpdateReceived(update);
    }

    @Override
    public Message sendDialog(Long chatId, Dialog<? extends AbstractMarkup> dialog) {
        return this.workerBot.sendDialog(chatId, dialog);
    }

    @Override
    public Message sendText(Long chatId, String text) {
        return this.workerBot.sendText(chatId, text);
    }

    @Override
    public Message sendPhoto(Long chatId, String photoName, InputStream photoInputStream) {
        return this.workerBot.sendPhoto(chatId, photoName, photoInputStream);
    }
}
