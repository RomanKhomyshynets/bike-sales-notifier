package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

abstract class AbstractButtonPropertiesBuilder<THIS extends AbstractButtonPropertiesBuilder,
        WORKER extends AbstractMarkupBuildingWorker,
        MARKUP_BUILDER extends AbstractMarkupBuilder,
        BUTTON_PROPERTIES_BUILDER extends AbstractButtonPropertiesBuilder,
        COMPONENT extends AbstractButton> {
    protected WORKER worker;
    protected MARKUP_BUILDER markupBuilder;

    protected AbstractButtonPropertiesBuilder(WORKER worker, MARKUP_BUILDER markupBuilder) {
        this.worker = worker;
        this.markupBuilder = markupBuilder;
    }

    @SuppressWarnings("unchecked")
    public BUTTON_PROPERTIES_BUILDER addButton(String text) {
        this.worker.addButton(text);
        return (BUTTON_PROPERTIES_BUILDER) this.markupBuilder.getButtonPropertiesBuilder();
    }

    @SuppressWarnings("unchecked")
    public BUTTON_PROPERTIES_BUILDER addButton(Long buttonId, String text) {
        this.worker.addButton(buttonId, text);
        return (BUTTON_PROPERTIES_BUILDER) this.markupBuilder.getButtonPropertiesBuilder();
    }

    public MARKUP_BUILDER endRow() {
        this.worker.endRow();
        return this.markupBuilder;
    }

    @SuppressWarnings("unchecked")
    public THIS withEmoji(String emojiShortCode, boolean isLeading) {
        this.worker.withEmoji(emojiShortCode, isLeading);
        return (THIS) this;
    }

    @SuppressWarnings("unchecked")
    public THIS withButtonGroup(ButtonGroup<COMPONENT> buttonGroup) {
        this.worker.withButtonGroup(buttonGroup);
        return (THIS) this;
    }

    @SuppressWarnings("unchecked")
    public THIS withActionListener(ActionListener<COMPONENT> actionListener) {
        this.worker.withActionListener(actionListener);
        return (THIS) this;
    }
}
