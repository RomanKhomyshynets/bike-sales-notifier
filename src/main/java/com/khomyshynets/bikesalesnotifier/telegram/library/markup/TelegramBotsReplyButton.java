package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;

class TelegramBotsReplyButton extends KeyboardButton implements TelegramBotsButton {

    TelegramBotsReplyButton() {
    }

    TelegramBotsReplyButton(KeyboardButton keyboardButton) {
        this.setText(keyboardButton.getText());
        this.setRequestLocation(keyboardButton.getRequestLocation());
        this.setRequestContact(keyboardButton.getRequestContact());
    }
}
