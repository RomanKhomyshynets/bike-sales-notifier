package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import com.khomyshynets.bikesalesnotifier.telegram.library.exceptions.TelegramComponentException;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

abstract class AbstractMarkupBuildingWorker<MARKUP extends AbstractMarkup,
        BUTTON extends AbstractButton> {

    protected List<List<? extends AbstractButton>> keyboard = new ArrayList<>();
    protected List<AbstractButton> buttonRow;
    protected MARKUP keyboardMarkup;
    protected AbstractButton button;
    protected List<AbstractButton> buttonGridList;
    private Class<BUTTON> buttonType;

    protected AbstractMarkupBuildingWorker(Class<BUTTON> buttonType) {
        this.buttonType = buttonType;
    }

    void startRow() {
        this.buttonRow = new ArrayList<>();
    }

    void endRow() {
        this.keyboard.add(this.buttonRow);
        this.buttonRow = null;
    }

    void addButton(String text) {
        addButton(null, text, this.buttonType);
    }

    void addButton(Long buttonId, String text) {
        addButton(buttonId, text, this.buttonType);
    }

    void addButtonGrid(List<String> buttonTexts, int columnAmount) {
        addButtonGrid(buttonTexts, columnAmount, this.buttonType);
    }

    void addButtonGrid(Map<Long, String> buttonInfo, int columnAmount) {
        addButtonGrid(buttonInfo, columnAmount, this.buttonType);
    }

    void withEmoji(String emojiShortCode, boolean isLeading) {
        this.button.setEmoji(new Emoji(emojiShortCode), isLeading);
    }

    @SuppressWarnings("unchecked")
    void withButtonGroup(ButtonGroup<BUTTON> buttonGroup) {
        this.button.addToButtonGroup(buttonGroup);
    }

    @SuppressWarnings("unchecked")
    void withActionListener(ActionListener<BUTTON> actionListener) {
        this.button.addActionListener(actionListener);
    }

    void withEmojiForEachButton(String emojiShortCode, boolean isLeading) {
        this.buttonGridList.forEach(button -> button.setEmoji(new Emoji(emojiShortCode), isLeading));
    }

    @SuppressWarnings("unchecked")
    void withButtonGroupForEachButton(ButtonGroup<BUTTON> buttonGroup) {
        buttonGroup.add((Collection<? extends BUTTON>) this.buttonGridList);
    }

    @SuppressWarnings("unchecked")
    void withActionListenerForEachButton(ActionListener<BUTTON> actionListener) {
        this.buttonGridList.forEach(button -> button.addActionListener(actionListener));
    }

    @SuppressWarnings("unchecked")
    void removeButton(Long buttonId) throws TelegramComponentException {
        Optional<BUTTON> button = this.keyboardMarkup.getButtonById(buttonId);
        if (this.keyboardMarkup.getKeyboard().isEmpty() || !button.isPresent()) {
            throw new TelegramComponentException("Button with ID #" + buttonId + " doesn't exist");
        }
        this.keyboardMarkup.getKeyboard()
                .forEach(row -> ((Collection) row).remove(button.get()));
    }

    void removeButtons(List<Long> buttonIds) {
        buttonIds.forEach(this::removeButton);
    }

    void removeRow(int rowNumber) throws TelegramComponentException {
        if (this.keyboardMarkup.getKeyboard().isEmpty()
                || this.keyboardMarkup.getKeyboard().size() < rowNumber + 1) {
            throw new TelegramComponentException("Button row #" + rowNumber + " doesn't exist");
        }
        this.keyboardMarkup.getKeyboard().remove(rowNumber);
    }

    MARKUP build() {
        return this.keyboardMarkup;
    }

    @SuppressWarnings("unchecked")
    protected void addButtonGrid(Map<Long, String> buttonInfo, int columnAmount, Class<? extends AbstractButton> buttonType) {
        this.buttonGridList = buttonInfo.entrySet()
                .stream()
                .map(buttonData -> createButtonReflective(buttonData.getKey(), buttonData.getValue(), buttonType)
                        .setKeyboardMarkup(this.keyboardMarkup))
                .collect(toList());
        addButtonGridToKeyboard(this.buttonGridList, columnAmount);
    }

    @SuppressWarnings("unchecked")
    protected void addButtonGrid(List<String> buttonTexts, int columnAmount, Class<? extends AbstractButton> buttonType) {
        this.buttonGridList = buttonTexts.stream()
                .map(buttonText -> createButtonReflective(null, buttonText, buttonType)
                        .setKeyboardMarkup(this.keyboardMarkup))
                .collect(toList());
        addButtonGridToKeyboard(this.buttonGridList, columnAmount);
    }

    private void addButtonGridToKeyboard(List<AbstractButton> buttonGridList, int columnAmount) {
        AtomicInteger index = new AtomicInteger();
        this.keyboard.addAll(buttonGridList.stream()
                .collect(groupingBy(button -> index.getAndIncrement() / columnAmount,
                        LinkedHashMap::new,
                        toList()))
                .values());
        ButtonGroup<AbstractButton> group = new ButtonGroup<>();
        group.add(buttonGridList);
    }

    @SuppressWarnings("unchecked")
    protected void addButton(Long buttonId, String buttonText, Class<? extends AbstractButton> buttonType) {
        this.button = createButtonReflective(buttonId, buttonText, buttonType)
                .setKeyboardMarkup(this.keyboardMarkup);
        this.buttonRow.add(this.button);
    }

    private AbstractButton createButtonReflective(Long buttonId, String buttonText, Class<? extends AbstractButton> buttonType)
            throws TelegramComponentException {
        try {
            return buttonId != null
                    ? buttonType.getConstructor(Long.class, String.class).newInstance(buttonId, buttonText)
                    : buttonType.getConstructor(String.class).newInstance(buttonText);
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new TelegramComponentException("Cannot create a component of the class " + buttonType.getName(), e);
        }
    }
}
