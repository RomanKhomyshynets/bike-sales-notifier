package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import com.khomyshynets.bikesalesnotifier.telegram.library.exceptions.TelegramComponentException;

import java.util.List;
import java.util.Map;

public class InlineCheckboxGridBuilder extends AbstractButtonGridBuilder<InlineCheckboxGridBuilder, InlineMarkupBuildingWorker,
        InlineMarkupBuilder, InlineButtonBuilder, InlineButtonGridBuilder, InlineCheckbox, InlineMarkup> {

    InlineCheckboxGridBuilder(InlineMarkupBuildingWorker worker, InlineMarkupBuilder markupBuilder) {
        super(worker, markupBuilder);
    }

    public InlineCheckboxGridBuilder addCheckboxGrid(List<String> checkboxTexts, int columnAmount) {
        this.worker.addCheckboxGrid(checkboxTexts, columnAmount);
        return this;
    }

    public InlineCheckboxGridBuilder addCheckboxGrid(Map<Long, String> checkboxInfo, int columnAmount) {
        this.worker.addCheckboxGrid(checkboxInfo, columnAmount);
        return this;
    }

    public InlineRadioButtonGridBuilder addRadioButtonGrid(List<String> radioButtonTexts, int columnAmount) {
        this.worker.addRadioButtonGrid(radioButtonTexts, columnAmount);
        return this.markupBuilder.getRadioButtonGridBuilder();
    }

    public InlineRadioButtonGridBuilder addRadioButtonGrid(Map<Long, String> radioButtonInfo, int columnAmount) {
        this.worker.addRadioButtonGrid(radioButtonInfo, columnAmount);
        return this.markupBuilder.getRadioButtonGridBuilder();
    }

    public InlineMarkupBuilder removeCheckbox(Long buttonId) throws TelegramComponentException {
        this.worker.removeCheckbox(buttonId);
        return this.markupBuilder;
    }

    public InlineMarkupBuilder removeRadioButton(Long buttonId) throws TelegramComponentException {
        this.worker.removeRadioButton(buttonId);
        return this.markupBuilder;
    }

    public InlineMarkupBuilder removeCheckboxes(List<Long> buttonIds) throws TelegramComponentException {
        this.worker.removeCheckboxes(buttonIds);
        return this.markupBuilder;
    }

    public InlineMarkupBuilder removeRadioButtons(List<Long> buttonIds) throws TelegramComponentException {
        this.worker.removeRadioButtons(buttonIds);
        return this.markupBuilder;
    }
}
