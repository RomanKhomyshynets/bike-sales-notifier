package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import org.telegram.telegrambots.meta.api.objects.games.CallbackGame;

public class InlineButton extends AbstractButton<InlineButton, InlineMarkup, TelegramBotsInlineButton> {

    public InlineButton(Long id, String text) {
        super(id, text);
    }

    public InlineButton(String text) {
        super(text);
    }

    @Override
    protected void createTelegramBotsButton() {
        this.telegramBotsButton = new TelegramBotsInlineButton();
        this.telegramBotsButton.setCallbackData(String.valueOf(this.id));
    }

    public String getUrl() {
        return this.telegramBotsButton.getUrl();
    }

    public InlineButton setUrl(String url) {
        this.telegramBotsButton.setUrl(url);
        return this;
    }

    public String getSwitchInlineQuery() {
        return this.telegramBotsButton.getSwitchInlineQuery();
    }

    public InlineButton setSwitchInlineQuery(String switchInlineQuery) {
        this.telegramBotsButton.setSwitchInlineQuery(switchInlineQuery);
        return this;
    }

    public String getSwitchInlineQueryCurrentChat() {
        return this.telegramBotsButton.getSwitchInlineQueryCurrentChat();
    }

    public InlineButton setSwitchInlineQueryCurrentChat(String switchInlineQueryCurrentChat) {
        this.telegramBotsButton.setSwitchInlineQueryCurrentChat(switchInlineQueryCurrentChat);
        return this;
    }

    public CallbackGame getCallbackGame() {
        return this.telegramBotsButton.getCallbackGame();
    }

    public InlineButton setCallbackGame(CallbackGame callbackGame) {
        this.telegramBotsButton.setCallbackGame(callbackGame);
        return this;
    }

    public Boolean getPay() {
        return this.telegramBotsButton.getPay();
    }

    public InlineButton setPay(Boolean pay) {
        this.telegramBotsButton.setPay(pay);
        return this;
    }
}
