package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

public class ReplyButtonPropertiesBuilder extends AbstractButtonPropertiesBuilder<ReplyButtonPropertiesBuilder,
        ReplyMarkupBuildingWorker, ReplyMarkupBuilder, ReplyButtonPropertiesBuilder, ReplyButton> {

    ReplyButtonPropertiesBuilder(ReplyMarkupBuildingWorker worker, ReplyMarkupBuilder markupBuilder) {
        super(worker, markupBuilder);
    }

    public ReplyButtonPropertiesBuilder withRequestOfContact(Boolean requestContact) {
        this.worker.withRequestOfContact(requestContact);
        return this;
    }

    public ReplyButtonPropertiesBuilder withRequestOfLocation(Boolean requestLocation) {
        this.worker.withRequestOfLocation(requestLocation);
        return this;
    }
}
