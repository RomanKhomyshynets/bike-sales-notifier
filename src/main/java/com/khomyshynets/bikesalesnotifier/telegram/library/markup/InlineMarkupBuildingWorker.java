package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import com.khomyshynets.bikesalesnotifier.telegram.library.exceptions.TelegramComponentException;
import org.telegram.telegrambots.meta.api.objects.games.CallbackGame;

import java.util.List;
import java.util.Map;
import java.util.Optional;

class InlineMarkupBuildingWorker extends AbstractMarkupBuildingWorker<InlineMarkup, InlineButton> {

    InlineMarkupBuildingWorker() {
        super(InlineButton.class);
        this.keyboardMarkup = new InlineMarkup()
                .setKeyboard(this.keyboard);
    }

    InlineMarkupBuildingWorker(InlineMarkup keyboardMarkup) {
        super(InlineButton.class);
        this.keyboardMarkup = keyboardMarkup;
        this.keyboard = keyboardMarkup.getKeyboard();
    }

    void addCheckbox(String text) {
        addButton(null, text, InlineCheckbox.class);
    }

    void addCheckbox(Long buttonId, String text) {
        addButton(buttonId, text, InlineCheckbox.class);
    }

    void addRadioButton(String text, ButtonGroup<InlineRadioButton> buttonGroup) {
        addRadioButton(null, text, buttonGroup);
    }

    @SuppressWarnings("unchecked")
    void addRadioButton(Long buttonId, String text, ButtonGroup<InlineRadioButton> buttonGroup) {
        addButton(buttonId, text, InlineRadioButton.class);
        this.button.setButtonGroup(buttonGroup);
    }

    void addCheckboxGrid(List<String> checkboxTexts, int columnAmount) {
        addButtonGrid(checkboxTexts, columnAmount, InlineCheckbox.class);
    }

    void addCheckboxGrid(Map<Long, String> checkboxInfo, int columnAmount) {
        addButtonGrid(checkboxInfo, columnAmount, InlineCheckbox.class);
    }

    void addRadioButtonGrid(List<String> radioButtonTexts, int columnAmount) {
        addButtonGrid(radioButtonTexts, columnAmount, InlineRadioButton.class);
        ((InlineRadioButton) this.buttonGridList.get(0)).setSelected(true);
    }

    void addRadioButtonGrid(Map<Long, String> radioButtonInfo, int columnAmount) {
        addButtonGrid(radioButtonInfo, columnAmount, InlineRadioButton.class);
        ((InlineRadioButton) this.buttonGridList.get(0)).setSelected(true);
    }

    void removeCheckbox(Long buttonId) throws TelegramComponentException {
        Optional<InlineCheckbox> checkbox = this.keyboardMarkup.getCheckboxById(buttonId);
        if (this.keyboardMarkup.getKeyboard().isEmpty() || !checkbox.isPresent()) {
            throw new TelegramComponentException("Checkbox with ID #" + buttonId + " doesn't exist");
        }
        this.keyboardMarkup.getKeyboard()
                .forEach(row -> row.remove(checkbox.get()));
    }

    void removeRadioButton(Long buttonId) throws TelegramComponentException {
        Optional<InlineRadioButton> radioButton = this.keyboardMarkup.getRadioButtonById(buttonId);
        if (this.keyboardMarkup.getKeyboard().isEmpty() || !radioButton.isPresent()) {
            throw new TelegramComponentException("RadioButton with ID #" + buttonId + " doesn't exist");
        }
        this.keyboardMarkup.getKeyboard()
                .forEach(row -> row.remove(radioButton.get()));
    }

    void removeCheckboxes(List<Long> buttonIds) throws TelegramComponentException {
        buttonIds.forEach(this::removeCheckbox);
    }

    void removeRadioButtons(List<Long> buttonIds) throws TelegramComponentException {
        buttonIds.forEach(this::removeRadioButton);
    }

    @Override
    void withButtonGroupForEachButton(ButtonGroup<InlineButton> buttonGroup) {
        super.withButtonGroupForEachButton(buttonGroup);
        if (this.buttonGridList.get(0).getClass().equals(InlineRadioButton.class)) {
            ((InlineRadioButton) buttonGridList.get(0)).setSelected(true);
        }
    }

    void withUrl(String url) {
        ((InlineButton) this.button).setUrl(url);
    }

    void withSwitchingInlineQuery(String switchInlineQuery) {
        ((InlineButton) this.button).setSwitchInlineQuery(switchInlineQuery);
    }

    void withSwitchingInlineQueryInCurrentChat(String switchInlineQueryCurrentChat) {
        ((InlineButton) this.button).setSwitchInlineQueryCurrentChat(switchInlineQueryCurrentChat);
    }

    void withCallbackGame(CallbackGame callbackGame) throws TelegramComponentException {
        if (this.buttonRow.size() > 1 || !this.keyboard.isEmpty()) {
            throw new TelegramComponentException("Button with callback game must be the first button in the first row");
        }
        ((InlineButton) this.button).setCallbackGame(callbackGame);
    }

    void withPayment(Boolean pay) throws TelegramComponentException {
        if (this.buttonRow.size() > 1 || !this.keyboard.isEmpty()) {
            throw new TelegramComponentException("Payment button must be the first button in the first row");
        }
        ((InlineButton) this.button).setPay(pay);
    }
}

