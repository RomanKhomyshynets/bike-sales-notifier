package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.objects.Update;

import static com.khomyshynets.bikesalesnotifier.telegram.library.markup.ButtonEmojiHelper.pasteButtonEmojiIntoText;

public class InlineCheckbox extends InlineSelectableComponent<InlineCheckbox> {

    public InlineCheckbox(Long buttonId, String text) {
        super(buttonId, text);
    }

    public InlineCheckbox(String text) {
        super(text);
    }

    @Override
    public InlineCheckbox setSelected(boolean selected) {
        this.setText(selected
                ? pasteButtonEmojiIntoText(ButtonEmojiHelper.clearSelectionEmojies(this),
                this.selectedEmoji, true)
                : pasteButtonEmojiIntoText(ButtonEmojiHelper.clearSelectionEmojies(this),
                this.unselectedEmoji, true));
        if (this.buttonGroup != null) {
            if (selected) {
                this.buttonGroup.addToSelected(this);
            } else {
                this.buttonGroup.removeFromSelected(this);
            }
        }
        this.selected = selected;
        return this;
    }

    @Override
    public void updateReceived(Update update, DefaultAbsSender bot) {
        super.updateReceived(update, bot);
        setSelected(!selected);
        updateMarkup(update, bot);
    }

    @Override
    protected void init() {
        selectedEmoji = new Emoji(":ballot_box_with_check:");
        unselectedEmoji = new Emoji(":white_large_square:");
        this.setText(pasteButtonEmojiIntoText(this.getText(), this.unselectedEmoji, true));
    }
}
