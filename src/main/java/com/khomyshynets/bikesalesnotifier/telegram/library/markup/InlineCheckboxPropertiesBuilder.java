package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

public class InlineCheckboxPropertiesBuilder extends AbstractButtonPropertiesBuilder<InlineCheckboxPropertiesBuilder,
        InlineMarkupBuildingWorker, InlineMarkupBuilder, InlineButtonPropertiesBuilder, InlineCheckbox> {

    InlineCheckboxPropertiesBuilder(InlineMarkupBuildingWorker worker, InlineMarkupBuilder markupBuilder) {
        super(worker, markupBuilder);
    }

    public InlineCheckboxPropertiesBuilder addCheckbox(String text) {
        this.worker.addCheckbox(text);
        return this;
    }

    public InlineCheckboxPropertiesBuilder addCheckbox(Long buttonId, String text) {
        this.worker.addCheckbox(buttonId, text);
        return this;
    }

    public InlineRadioButtonPropertiesBuilder addRadioButton(String text, ButtonGroup<InlineRadioButton> buttonGroup) {
        this.worker.addRadioButton(text, buttonGroup);
        return this.markupBuilder.getRadioButtonPropertiesBuilder();
    }

    public InlineRadioButtonPropertiesBuilder addRadioButton(Long buttonId, String text, ButtonGroup<InlineRadioButton> buttonGroup) {
        this.worker.addRadioButton(buttonId, text, buttonGroup);
        return this.markupBuilder.getRadioButtonPropertiesBuilder();
    }
}
