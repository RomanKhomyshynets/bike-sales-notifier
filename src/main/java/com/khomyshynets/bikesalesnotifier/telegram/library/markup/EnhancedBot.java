package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.InputStream;

interface EnhancedBot {

    void initialize(Update initialUpdate);

    Message sendDialog(Long chatId, Dialog<? extends AbstractMarkup> dialog);

    Message sendText(Long chatId, String text);

    Message sendPhoto(Long chatId, String photoName, InputStream photoInputStream);
}
