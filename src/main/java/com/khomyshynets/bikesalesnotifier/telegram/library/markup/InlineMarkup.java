package com.khomyshynets.bikesalesnotifier.telegram.library.markup;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class InlineMarkup extends AbstractMarkup<InlineMarkup, InlineButton, InlineKeyboardMarkup> {

    public InlineMarkup() {
        super(InlineButton.class);
        this.telegramBotsMarkup = new InlineKeyboardMarkup();
    }

    public InlineMarkup(InlineKeyboardMarkup telegramBotsMarkup) {
        super(InlineButton.class, telegramBotsMarkup);
    }

    @Override
    protected void fillKeyboard() {
        this.keyboard = this.telegramBotsMarkup.getKeyboard()
                .stream()
                .map(row -> row.stream()
                        .map(telegramBotsButton -> new InlineButton(telegramBotsButton.getText())
                                .setTelegramBotsButton(new TelegramBotsInlineButton(telegramBotsButton)))
                        .collect(toList()))
                .collect(toList());
    }

    @Override
    protected void fillTelegramBotsKeyboard() {
        this.telegramBotsMarkup.setKeyboard(this.keyboard.stream()
                .map(row -> row.stream()
                        .map(abstractButton -> (InlineKeyboardButton) abstractButton.getTelegramBotsButton())
                        .collect(toList()))
                .collect(toList()));
    }

    public InlineMarkupBuilder getBuilder() {
        return new InlineMarkupBuilder(new InlineMarkupBuildingWorker(this));
    }

    public Optional<InlineCheckbox> getCheckboxById(Long buttonId) {
        return getAllCheckboxes()
                .filter(checkbox -> checkbox.getId().equals(buttonId))
                .findFirst();
    }

    public Optional<InlineRadioButton> getRadioButtonById(Long buttonId) {
        return getAllRadioButtons()
                .filter(radioButton -> radioButton.getId().equals(buttonId))
                .findFirst();
    }

    public Optional<InlineCheckbox> getCheckboxByText(String buttonText) {
        return getAllCheckboxes()
                .filter(checkbox -> checkbox.getText().equals(buttonText))
                .findFirst();
    }

    public Optional<InlineRadioButton> getRadioButtonByText(String buttonText) {
        return getAllRadioButtons()
                .filter(radioButton -> radioButton.getText().equals(buttonText))
                .findFirst();
    }

    public Stream<InlineCheckbox> getAllCheckboxes() {
        return this.keyboard
                .stream()
                .flatMap(Collection::stream)
                .filter(button -> button.getClass().equals(InlineCheckbox.class))
                .map(button -> (InlineCheckbox) button);
    }

    public Stream<InlineRadioButton> getAllRadioButtons() {
        return this.keyboard
                .stream()
                .flatMap(Collection::stream)
                .filter(button -> button.getClass().equals(InlineRadioButton.class))
                .map(button -> (InlineRadioButton) button);
    }

    public void forEachCheckbox(Consumer<InlineCheckbox> consumer) {
        getAllCheckboxes().forEach(consumer);
    }

    public void forEachRadioButton(Consumer<InlineRadioButton> consumer) {
        getAllRadioButtons().forEach(consumer);
    }
}
