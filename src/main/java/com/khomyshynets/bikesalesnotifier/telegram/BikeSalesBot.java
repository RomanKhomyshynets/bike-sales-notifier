package com.khomyshynets.bikesalesnotifier.telegram;

import com.khomyshynets.bikesalesnotifier.dao.PelotonNetDao;
import com.khomyshynets.bikesalesnotifier.entities.Item;
import com.khomyshynets.bikesalesnotifier.telegram.library.markup.*;
import com.khomyshynets.bikesalesnotifier.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

@Component
public class BikeSalesBot extends EnhancedLongPollingBot {
    @Autowired
    PelotonNetDao pelotonNetDao;

    @Override
    public String getBotUsername() {
        return Constants.TELEGRAM_BOT_USERNAME;
    }

    @Override
    public String getBotToken() {
        return Constants.TELEGRAM_BOT_TOKEN;
    }

    @Override
    public void initialize(Update initialUpdate) {
        // get IDs and names for clickable components
        Map<Long, String> data = pelotonNetDao.getSections()
                .stream()
                .flatMap(section -> pelotonNetDao.getCategoriesBySection(section)
                        .stream())
                .collect(toMap(Item::getId, Item::getName));


        // create inline markup using newly created builder
        InlineMarkupBuilder inlineBuilder = Builders.inlineMarkupBuilder();
        InlineMarkup inlineMarkup = inlineBuilder.addCheckboxGrid(data, 2)
                .withEmojiForEachButton(":one:", false)
                .startRow()
                .addButton("Back")
                .addButton("Forward")
                .endRow()
                .build();

        // add buttons to existing inline markup, using its already existing builder through getter
        inlineMarkup.getBuilder()
                .startRow()
                .addButton("Spare")
                .endRow()
                .build();

        // create clone of existing data map with different IDs (to ensure ID uniqueness)
        Map<Long, String> data2 = data.keySet()
                .stream()
                .collect(toMap(key -> key * 100, data::get));

        // add radio-buttons to existing inline markup using link on its existing builder; assign button group to these radio-buttons
        ButtonGroup<InlineRadioButton> b1 = new ButtonGroup<>();
        inlineMarkup = inlineBuilder.addRadioButtonGrid(data2, 1)
                .withEmojiForEachButton(":two:", false)
                .withButtonGroupForEachButton(b1)
                .build();

        // create reply markup with builder
        ReplyMarkup replyMarkup = Builders.replyMarkupBuilder()
                .addButtonGrid(data, 2)
                .startRow()
                .addButton("Reply")
                .endRow()
                .build();

        // create inline markup manually without builders
        InlineMarkup manualMarkup = new InlineMarkup();
        List<InlineCheckbox> manualRow = new ArrayList<>();
        manualRow.add(new InlineCheckbox("Manual1"));
        manualRow.add(new InlineCheckbox("Manual2"));
        List<List<InlineCheckbox>> manualKeyboard = new ArrayList<>();
        manualKeyboard.add(manualRow);
        manualMarkup.setKeyboard(manualKeyboard);

        Dialog<InlineMarkup> inlineDialog = new Dialog<>("Categories", inlineMarkup);
        Dialog<ReplyMarkup> replyDialog = new Dialog<>("Categories", replyMarkup);

        // Telegram allows to send only one type of keyboard per message
        sendDialog(initialUpdate.getMessage().getChatId(), inlineDialog);
//        sendDialog(initialUpdate.getMessage().getChatId(), replyDialog);
    }
}
