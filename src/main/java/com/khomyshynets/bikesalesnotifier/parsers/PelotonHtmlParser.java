package com.khomyshynets.bikesalesnotifier.parsers;

import com.khomyshynets.bikesalesnotifier.entities.Category;
import com.khomyshynets.bikesalesnotifier.entities.Topic;
import com.khomyshynets.bikesalesnotifier.utils.JsoupConnector;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.khomyshynets.bikesalesnotifier.utils.Constants.REQUEST_INTERVAL_IN_MILLIS;
import static com.khomyshynets.bikesalesnotifier.utils.PelotonUrlUtils.buildItemUrlFromDomElement;
import static com.khomyshynets.bikesalesnotifier.utils.PelotonUrlUtils.getItemIdFromUrl;
import static java.util.stream.Collectors.toList;

@Component
public class PelotonHtmlParser {
    private JsoupConnector jsoupConnector;
    private final DateTimeFormatter russianDateFormat = DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.forLanguageTag("ru"));
    private final DateTimeFormatter russianDateTimeFormat = DateTimeFormatter.ofPattern("dd MMM yyyy, HH:mm", Locale.forLanguageTag("ru"));

    @Autowired
    public PelotonHtmlParser(JsoupConnector jsoupConnector) {
        this.jsoupConnector = jsoupConnector;
    }

    public boolean isNewTopic(Topic topic) {
        return ChronoUnit.SECONDS.between(LocalDateTime.now(), topic.getCreated()) <= REQUEST_INTERVAL_IN_MILLIS;
    }

    public boolean isUpdatedTopic(Topic topic) {
        return ChronoUnit.SECONDS.between(LocalDateTime.now(), topic.getUpdated()) <= REQUEST_INTERVAL_IN_MILLIS;
    }

    private Category getCategoryFromDomElement(Element anchor) {
        Category category = new Category();
        String url = buildItemUrlFromDomElement(anchor);
        category.setUrl(url);
        category.setId(getItemIdFromUrl(url));
        category.setName(anchor.text().replaceFirst("продам ", ""));
        Element sectionAnchor = isInSearchView(anchor) ? jsoupConnector.getJsoupDocument(url).selectFirst("li.icon-home > a:nth-of-type(3)")
                : anchor.parents().select("li.icon-home > a:nth-of-type(3)").first();
        Category section = sectionAnchor != anchor ? getCategoryFromDomElement(sectionAnchor) : null;
        category.setSection(section);
        return category;
    }

    private Topic getTopicFromDomElement(Element dl) {
        Element topicAnchor = dl.selectFirst("dt > a:nth-of-type(1)");
        Element topicStarterAnchor = dl.selectFirst("dt > a:nth-of-type(2)");
        Topic topic = new Topic();
        String url = buildItemUrlFromDomElement(topicAnchor);
        topic.setUrl(url);
        topic.setId(getItemIdFromUrl(url));
        topic.setName(topicAnchor.text());
        topic.setTopicStarter(topicStarterAnchor.text());
        LocalDateTime created = parseDateTime(dl.selectFirst("dt").text().replaceAll(".*» ", ""));
        topic.setCreated(created);
        dl.select("dd.lastpost span dfn, dd.lastpost span a").remove();
        LocalDateTime updated = parseDateTime(dl.selectFirst("dd.lastpost span").text());
        topic.setUpdated(updated);
        Element categoryAnchor = isInSearchView(dl) ? dl.selectFirst("dt > a:nth-of-type(3)") : dl.parents().select("#page-body a").first();
        topic.setCategory(getCategoryFromDomElement(categoryAnchor));
        return topic;
    }

    private boolean isInSearchView(Element element) {
        return element.parents().select("body").hasClass("section-search");
    }

    public List<Category> parseSections(String[] sectionUrls) {
        return Arrays.stream(sectionUrls)
                .map(url -> new Category(getItemIdFromUrl(url),
                        jsoupConnector.getJsoupDocument(url).select("h2 > a").text(),
                        url,
                        null))
                .collect(toList());
    }

    public List<Category> parseCategories(String sectionUrl) {
        return jsoupConnector.getJsoupDocument(sectionUrl)
                .select("ul.forums a:first-child")
                .stream()
                .map(this::getCategoryFromDomElement)
                .collect(toList());
    }

    public List<Topic> parseTopics(String url) {
        return jsoupConnector.getJsoupDocument(url)
                .select("ul.topics dl")
                .stream()
                .map(this::getTopicFromDomElement)
                .collect(toList());
    }

    private LocalDateTime parseDateTime(String htmlDateTime) {
        Matcher matcher = Pattern.compile("(\\d+)( минут.*)").matcher(htmlDateTime);
        if (matcher.find()) {
            int minutesAgo = Integer.parseInt(matcher.replaceFirst("$1"));
            return LocalDateTime.now().minusMinutes(minutesAgo).truncatedTo(ChronoUnit.MINUTES);
        }
        htmlDateTime = htmlDateTime
                .replaceFirst(" в форуме.*", "")
                .replaceFirst("Вчера", LocalDate.now().minusDays(1).format(russianDateFormat))
                .replaceFirst("Сегодня", LocalDate.now().format(russianDateFormat))
                .replaceFirst("май", "мая");
        return LocalDateTime.parse(htmlDateTime, russianDateTimeFormat);
    }
}
