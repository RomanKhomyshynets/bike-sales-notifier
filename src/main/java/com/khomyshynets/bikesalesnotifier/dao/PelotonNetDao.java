package com.khomyshynets.bikesalesnotifier.dao;

import com.khomyshynets.bikesalesnotifier.entities.Category;
import com.khomyshynets.bikesalesnotifier.entities.Topic;
import com.khomyshynets.bikesalesnotifier.parsers.PelotonHtmlParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.khomyshynets.bikesalesnotifier.utils.Constants.PELOTON_SECTION_URLS;
import static com.khomyshynets.bikesalesnotifier.utils.PelotonUrlUtils.buildSearchUrl;

@Component
public class PelotonNetDao {
    private final PelotonHtmlParser pelotonHtmlParser;

    @Autowired
    public PelotonNetDao(PelotonHtmlParser pelotonHtmlParser) {
        this.pelotonHtmlParser = pelotonHtmlParser;
    }

    public List<Category> getSections() {
        return pelotonHtmlParser.parseSections(PELOTON_SECTION_URLS);
    }

    public List<Category> getCategoriesBySection(Category section) {
        return pelotonHtmlParser.parseCategories(section.getUrl());
    }

    public List<Topic> getTopicsByCategory(Category category) {
        return pelotonHtmlParser.parseTopics(category.getUrl());
    }

    public List<Topic> getTopicsByTopicStarter(String topicStarter) {
        return getTopicsByCategoriesAndTopicStarter(getSections(), topicStarter);
    }

    public List<Topic> getTopicsByCategoriesAndTopicStarter(List<Category> categories, String topicStarter) {
        String searchUrl = buildSearchUrl(categories, topicStarter);
        return pelotonHtmlParser.parseTopics(searchUrl);
    }
}