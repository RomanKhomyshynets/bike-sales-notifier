package com.khomyshynets.bikesalesnotifier.entities;

public class Category extends Item {
    private Category section;

    public Category() {
    }

    public Category(Long id, String name, String url, Category section) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.section = section;
    }

    public Category getSection() {
        return section;
    }

    public Category setSection(Category section) {
        this.section = section;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Category category = (Category) o;

        return section != null ? section.equals(category.section) : category.section == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (section != null ? section.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Category{" +
                "section=" + section +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}

