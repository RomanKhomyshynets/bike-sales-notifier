package com.khomyshynets.bikesalesnotifier.entities;

import java.time.LocalDateTime;

public class Topic extends Item {
    private String topicStarter;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Category category;

    public Topic() {
    }

    public Topic(Long id, String name, String url, Category category) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.category = category;
    }

    public String getTopicStarter() {
        return topicStarter;
    }

    public Topic setTopicStarter(String topicStarter) {
        this.topicStarter = topicStarter;
        return this;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public Topic setCreated(LocalDateTime created) {
        this.created = created;
        return this;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public Topic setUpdated(LocalDateTime updated) {
        this.updated = updated;
        return this;
    }

    public Category getCategory() {
        return category;
    }

    public Topic setCategory(Category category) {
        this.category = category;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Topic that = (Topic) o;

        if (!topicStarter.equals(that.topicStarter)) return false;
        if (!created.equals(that.created)) return false;
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) return false;
        return category.equals(that.category);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + topicStarter.hashCode();
        result = 31 * result + created.hashCode();
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + category.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Topic{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", topicStarter='" + topicStarter + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                ", category=" + category +
                '}';
    }
}
