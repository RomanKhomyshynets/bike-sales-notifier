package com.khomyshynets.bikesalesnotifier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.telegram.telegrambots.ApiContextInitializer;

@EnableRetry
@EnableScheduling
@SpringBootApplication
public class BikeSalesNotifierApplication {
    public static void main(String[] args) {
        ApiContextInitializer.init();
        SpringApplication.run(BikeSalesNotifierApplication.class, args);
    }
}
