package com.khomyshynets.bikesalesnotifier.exceptions;

public class JsoupConnectException extends RuntimeException {
    public JsoupConnectException() {
    }

    public JsoupConnectException(String message) {
        super(message);
    }

    public JsoupConnectException(String message, Throwable cause) {
        super(message, cause);
    }

    public JsoupConnectException(Throwable cause) {
        super(cause);
    }

    public JsoupConnectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
