package com.khomyshynets.bikesalesnotifier.utils;

import com.khomyshynets.bikesalesnotifier.exceptions.JsoupConnectException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class JsoupConnector {
    @Retryable(
            value = {IOException.class},
            maxAttempts = 10, backoff = @Backoff(delay = 1000))
    public Document getJsoupDocument(String url) throws JsoupConnectException {
        Document doc;
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            throw new JsoupConnectException("Problems with connection to the requested forum. Please, check its availability", e);
        }
        return doc;
    }
}