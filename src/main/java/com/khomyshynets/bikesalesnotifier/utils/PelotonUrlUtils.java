package com.khomyshynets.bikesalesnotifier.utils;

import com.khomyshynets.bikesalesnotifier.entities.Category;
import org.jsoup.nodes.Element;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;

import static java.lang.Long.parseLong;

public class PelotonUrlUtils {
    private static final String BASE_URL = "http://peloton.com.ua/forum";

    public static Long getItemIdFromUrl(String url) {
        final Map<String, List<String>> paramMap = UriComponentsBuilder.fromUriString(url).build().getQueryParams();
        if (paramMap.containsKey("t")) {
            return parseLong(paramMap.get("t").get(0));
        } else {
            return parseLong(paramMap.get("f").get(0));
        }
    }

    public static String buildItemUrlFromDomElement(Element element) {
        return BASE_URL + element.attr("href").replaceAll("^\\.|&sid.*", "");
    }

    public static String buildSearchUrl(List<Category> categories, String topicStarter) {
        return UriComponentsBuilder.fromUriString(BASE_URL + "/search.php")
                .queryParam("author", topicStarter)
                .queryParam("fid%5B%5D", (Object[]) categories.stream()
                        .map(category -> String.valueOf(category.getId()))
                        .toArray(String[]::new))
                .queryParam("sr", "topics")
                .build().toUriString();
    }
}