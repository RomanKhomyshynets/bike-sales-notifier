package com.khomyshynets.bikesalesnotifier;

import com.khomyshynets.bikesalesnotifier.dao.PelotonNetDao;
import com.khomyshynets.bikesalesnotifier.entities.Category;
import com.khomyshynets.bikesalesnotifier.entities.Topic;
import com.khomyshynets.bikesalesnotifier.parsers.PelotonHtmlParser;
import com.khomyshynets.bikesalesnotifier.utils.JsoupConnector;
import org.jsoup.Jsoup;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

import static com.khomyshynets.bikesalesnotifier.utils.PelotonUrlUtils.getItemIdFromUrl;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PelotonNetDaoTest {
    private JsoupConnector jsoupConnector;
    private PelotonHtmlParser pelotonHtmlParser;
    private PelotonNetDao pelotonNetDao;

    private static final String MTB_SECTION_FILE = "src/test/resources/mtb-section.html";
    private static final String ROAD_SECTION_FILE = "src/test/resources/road-section.html";
    private static final String ROAD_FULL_BIKES_CATEGORY_FILE = "src/test/resources/road-full-bikes-category.html";
    private static final String ROAD_EXAMPLE_TOPIC_FILE = "src/test/resources/roadbike-example-topic.html";
    private static final String SEARCH_BY_TS_ALL_SECTIONS_FILE = "src/test/resources/search-by-ts-all-sections.html";
    private static final String SEARCH_BY_TS_ROAD_CATEGORY_FILE = "src/test/resources/search-by-ts-roadbike-category.html";

    private static final String SEARCH_BY_TS_AND_ROAD_CATEGORY_URL = "http://peloton.com.ua/forum/search.php?author=кеша31&fid%5B%5D=220&sr=topics";
    private static final String SEARCH_BY_TS_ALL_SECTIONS_URL = "http://peloton.com.ua/forum/search.php?author=кеша31&fid%5B%5D=220&fid%5B%5D=260&sr=topics";
    private static final String TOPIC_STARTER = "кеша31";

    private static Category roadSection;
    private static Category mtbSection;
    private static Category roadCategoryFirst;
    private static Category roadCategoryLast;
    private static Category mtbCategoryFirst;
    private static Category mtbCategoryLast;
    private static Topic roadTopic;
    private static Topic topicByTopicStarter_1;
    private static Topic topicByTopicStarter_2;

    @Before
    public void injectMocks() {
        jsoupConnector = mock(JsoupConnector.class);
        pelotonHtmlParser = spy(new PelotonHtmlParser(jsoupConnector));
        pelotonNetDao = new PelotonNetDao(pelotonHtmlParser);
    }

    @BeforeClass
    public static void setUp() {
        roadSection = new Category();
        roadSection.setId(220L)
                .setName("продажа б/у техники ШОССЕ")
                .setUrl("http://peloton.com.ua/forum/viewforum.php?f=220");

        mtbSection = new Category();
        mtbSection.setId(260L)
                .setName("МТВ - барахолка")
                .setUrl("http://peloton.com.ua/forum/viewforum.php?f=260");

        roadCategoryFirst = new Category();
        roadCategoryFirst.setSection(roadSection)
                .setId(221L)
                .setName("Б/У велосипеды ШОССЕ")
                .setUrl("http://peloton.com.ua/forum/viewforum.php?f=221");

        roadCategoryLast = new Category();
        roadCategoryLast.setSection(roadSection)
                .setId(231L)
                .setName("педали/шипы етс ШОССЕ")
                .setUrl("http://peloton.com.ua/forum/viewforum.php?f=231");

        mtbCategoryFirst = new Category();
        mtbCategoryFirst.setSection(mtbSection)
                .setId(262L)
                .setName("Б/У велосипеды")
                .setUrl("http://peloton.com.ua/forum/viewforum.php?f=262");

        mtbCategoryLast = new Category();
        mtbCategoryLast.setSection(mtbSection)
                .setId(176L)
                .setName("Продажа и покупка MTB , BMX и другой велотехники")
                .setUrl("http://peloton.com.ua/forum/viewforum.php?f=176");

        roadTopic = new Topic();
        roadTopic.setCategory(roadCategoryFirst)
                .setCreated(LocalDateTime.of(2018, Month.SEPTEMBER, 29, 17, 53))
                .setUpdated(LocalDateTime.now().minusDays(1).withHour(18).withMinute(43).truncatedTo(ChronoUnit.MINUTES))
                .setTopicStarter("taras_antares")
                .setId(50753L)
                .setName("Шоссейный велосипед Giant TCR Advanced 2 2018")
                .setUrl("http://peloton.com.ua/forum/viewtopic.php?f=221&t=50753");

        topicByTopicStarter_1 = new Topic();
        topicByTopicStarter_1.setCategory(roadCategoryFirst)
                .setCreated(LocalDateTime.of(2017, Month.NOVEMBER, 3, 20, 15))
                .setUpdated(LocalDateTime.of(2018, Month.AUGUST, 9, 13, 42))
                .setTopicStarter(TOPIC_STARTER)
                .setId(44770L)
                .setName("Велосипед BLR TROPIX .")
                .setUrl("http://peloton.com.ua/forum/viewtopic.php?f=221&t=44770");

        topicByTopicStarter_2 = new Topic();
        topicByTopicStarter_2.setCategory(roadCategoryLast)
                .setCreated(LocalDateTime.of(2018, Month.AUGUST, 29, 14, 13))
                .setUpdated(LocalDateTime.of(2018, Month.SEPTEMBER, 25, 16, 23))
                .setTopicStarter(TOPIC_STARTER)
                .setId(50260L)
                .setName("Шипы LOOK.")
                .setUrl("http://peloton.com.ua/forum/viewtopic.php?f=231&t=50260");
    }

    @Test
    public void getSections() throws IOException {
        when(jsoupConnector.getJsoupDocument(roadSection.getUrl()))
                .thenReturn(Jsoup.parse(new File(ROAD_SECTION_FILE), "UTF-8"));
        when(jsoupConnector.getJsoupDocument(mtbSection.getUrl()))
                .thenReturn(Jsoup.parse(new File(MTB_SECTION_FILE), "UTF-8"));
        assertEquals(Arrays.asList(roadSection, mtbSection), pelotonNetDao.getSections());
    }

    @Test
    public void getCategories() throws IOException {
        when(jsoupConnector.getJsoupDocument(roadSection.getUrl()))
                .thenReturn(Jsoup.parse(new File(ROAD_SECTION_FILE), "UTF-8"));
        when(jsoupConnector.getJsoupDocument(mtbSection.getUrl()))
                .thenReturn(Jsoup.parse(new File(MTB_SECTION_FILE), "UTF-8"));
        List<Category> roadCategories = pelotonNetDao.getCategoriesBySection(roadSection);
        List<Category> MTBCategories = pelotonNetDao.getCategoriesBySection(mtbSection);

        assertTrue(roadCategories.stream().allMatch(category -> category.getSection().equals(roadSection)));
        assertTrue(MTBCategories.stream().allMatch(category -> category.getSection().equals(mtbSection)));

        assertEquals(11, roadCategories.size());
        assertEquals(13, MTBCategories.size());

        assertEquals(roadCategoryFirst, roadCategories.get(0));
        assertEquals(roadCategoryLast, roadCategories.get(roadCategories.size() - 1));
        assertEquals(mtbCategoryFirst, MTBCategories.get(0));
        assertEquals(mtbCategoryLast, MTBCategories.get(MTBCategories.size() - 1));
    }

    @Test
    public void getTopicsByCategory() throws IOException {
        when(jsoupConnector.getJsoupDocument(anyString()))
                .thenReturn(Jsoup.parse(new File(ROAD_FULL_BIKES_CATEGORY_FILE), "UTF-8"));
        List<Topic> topicList = pelotonNetDao.getTopicsByCategory(roadCategoryFirst);

        assertTrue(topicList.stream().allMatch(topic -> topic.getCategory().equals(roadCategoryFirst)));
        assertTrue(topicList.stream().allMatch(topic -> topic.getCategory().getSection().equals(roadSection)));

        assertEquals(25, topicList.size());

        assertEquals(roadTopic, topicList.get(1));
    }

    @Test
    public void getTopicsByCategoriesAndTopicStarter() throws IOException {
        when(jsoupConnector.getJsoupDocument(anyString()))
                .thenReturn(Jsoup.parse(new File(ROAD_SECTION_FILE), "UTF-8"));
        when(jsoupConnector.getJsoupDocument(SEARCH_BY_TS_AND_ROAD_CATEGORY_URL))
                .thenReturn(Jsoup.parse(new File(SEARCH_BY_TS_ROAD_CATEGORY_FILE), "UTF-8"));
        List<Topic> topicList = pelotonNetDao.getTopicsByCategoriesAndTopicStarter(singletonList(roadSection), TOPIC_STARTER);

        assertTrue(topicList.stream().allMatch(topic -> topic.getCategory().equals(roadCategoryFirst)));
        assertTrue(topicList.stream().allMatch(topic -> topic.getCategory().getSection().equals(roadSection)));

        assertEquals(25, topicList.size());

        assertEquals(topicByTopicStarter_1, topicList.get(9));
    }

    @Test
    public void getTopicsByTopicStarterTest() throws IOException {
        when(jsoupConnector.getJsoupDocument(anyString()))
                .thenAnswer(invocation -> Jsoup.parse(new File((!invocation.getArgument(0).equals(SEARCH_BY_TS_ALL_SECTIONS_URL)
                                && getItemIdFromUrl(invocation.getArgument(0)) >= 220
                                && getItemIdFromUrl(invocation.getArgument(0)) < 260)
                                ? ROAD_SECTION_FILE
                                : MTB_SECTION_FILE),
                        "UTF-8"));
        when(jsoupConnector.getJsoupDocument(roadSection.getUrl()))
                .thenReturn(Jsoup.parse(new File(ROAD_SECTION_FILE), "UTF-8"));
        when(jsoupConnector.getJsoupDocument(mtbSection.getUrl()))
                .thenReturn(Jsoup.parse(new File(MTB_SECTION_FILE), "UTF-8"));
        when(jsoupConnector.getJsoupDocument(SEARCH_BY_TS_ALL_SECTIONS_URL))
                .thenReturn(Jsoup.parse(new File(SEARCH_BY_TS_ALL_SECTIONS_FILE), "UTF-8"));
        List<Topic> topicList = pelotonNetDao.getTopicsByTopicStarter(TOPIC_STARTER);

        assertEquals(3, topicList.stream()
                .filter(topic -> topic.getCategory().equals(roadCategoryFirst))
                .collect(toList()).size());
        assertEquals(1, topicList.stream()
                .filter(topic -> topic.getCategory().equals(roadCategoryLast))
                .collect(toList()).size());

        assertEquals(22, topicList.stream()
                .filter(topic -> topic.getCategory().getSection().equals(roadSection))
                .collect(toList()).size());
        assertEquals(3, topicList.stream()
                .filter(topic -> topic.getCategory().getSection().equals(mtbSection))
                .collect(toList()).size());

        assertEquals(25, topicList.size());

        assertEquals(topicByTopicStarter_2, topicList.get(9));
    }
}
