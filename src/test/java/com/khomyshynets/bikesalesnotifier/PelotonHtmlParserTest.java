package com.khomyshynets.bikesalesnotifier;

import com.khomyshynets.bikesalesnotifier.parsers.PelotonHtmlParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class PelotonHtmlParserTest {

    @InjectMocks
    private PelotonHtmlParser pelotonHtmlParser;

    @Test
    public void parseDateTime() {
        String htmlDateTime = "12 июл 2018, 09:47";
        String htmlDateTime1 = "12 ноя 2018, 09:47";
        String htmlDateTime2 = "15 минут назад";
        String htmlDateTime3 = "9 минут назад";
        String htmlDateTime4 = "Вчера, 15:45";
        String htmlDateTime5 = "Сегодня, 16:00";
        String htmlDateTime6 = "15 минут назад в форуме продам Б/У велосипеды ШОССЕ";
        String htmlDateTime7 = "07 май 2018, 19:20";
        try {
            Method parseDateTime = pelotonHtmlParser.getClass().getDeclaredMethod("parseDateTime", String.class);
            parseDateTime.setAccessible(true);
            assertEquals(LocalDateTime.of(2018, Month.JULY, 12, 9, 47), parseDateTime.invoke(pelotonHtmlParser, htmlDateTime));
            assertEquals(LocalDateTime.of(2018, Month.NOVEMBER, 12, 9, 47), parseDateTime.invoke(pelotonHtmlParser, htmlDateTime1));
            assertEquals(LocalDateTime.now().withSecond(0).withNano(0).minusMinutes(15), parseDateTime.invoke(pelotonHtmlParser, htmlDateTime2));
            assertEquals(LocalDateTime.now().withSecond(0).withNano(0).minusMinutes(9), parseDateTime.invoke(pelotonHtmlParser, htmlDateTime3));
            assertEquals(LocalDateTime.now().minusDays(1).withHour(15).withMinute(45).withSecond(0).withNano(0), parseDateTime.invoke(pelotonHtmlParser, htmlDateTime4));
            assertEquals(LocalDateTime.now().withHour(16).withMinute(0).withSecond(0).withNano(0), parseDateTime.invoke(pelotonHtmlParser, htmlDateTime5));
            assertEquals(LocalDateTime.now().withSecond(0).withNano(0).minusMinutes(15), parseDateTime.invoke(pelotonHtmlParser, htmlDateTime6));
            assertEquals(LocalDateTime.of(2018, Month.MAY, 7, 19, 20), parseDateTime.invoke(pelotonHtmlParser, htmlDateTime7));

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            fail();
        }
    }
}
